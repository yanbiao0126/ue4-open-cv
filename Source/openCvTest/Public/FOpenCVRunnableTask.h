// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TestActor.h"

/**
 * 
 */
class OPENCVTEST_API FOpenCVRunnableTask: public FRunnable
{
public:
	FOpenCVRunnableTask(FString threadName, class ATestActor* openCVActor):ThreadName(threadName),OpenCVActor(openCVActor){};
	~FOpenCVRunnableTask();

	virtual bool Init() override;
	virtual uint32 Run() override;
	virtual void Exit() override;

	FString ThreadName;

	class ATestActor* OpenCVActor;
	
private:
	FCriticalSection CriticalSection;
};
