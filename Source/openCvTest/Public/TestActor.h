// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/videoio.hpp"
#include "Runtime/Engine/Public/Rendering/Texture2DResource.h"
#include "opencv2/face.hpp"
#include "math.h"
#include "FOpenCVRunnableTask.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TestActor.generated.h"

DECLARE_DYNAMIC_DELEGATE_OneParam(FOnVideoTextureUpdate,UTexture2D*,VideoTexture);

USTRUCT(BlueprintType)
struct FAIUsers
{
	GENERATED_BODY()
	UPROPERTY(BlueprintReadOnly, Category = "Mantra")
	int ID;
	UPROPERTY(BlueprintReadOnly, Category = "Mantra")
	float XX;
	UPROPERTY(BlueprintReadOnly, Category = "Mantra")
	float YY;
};

UCLASS()
class OPENCVTEST_API ATestActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATestActor();
	~ATestActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//用于指定纹理对象的更新区域
	FUpdateTextureRegion2D* VideoUpdateTextureRegion;

	//4.更新纹理区域
	void UpdateTextureRegions(UTexture2D* Texture, int32 MipIndex, uint32 NumRegions, FUpdateTextureRegion2D* Regions,uint32 SrcPitch, uint32 SrcBpp, uint8* SrcData, bool bFreeData);

private:

public:
	// // TSharedPtr<FOpenCVRunnableTask> RunnableTask;
	// FOpenCVRunnableTask* RunnableTask;
	// FRunnableThread* RunnableThread;
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category="检测对象")
	TArray<FAIUsers> AiUsers;
	
	cv::Mat dst;
	cv::Mat dst1;
	cv::Mat image;
	cv::Mat imagel;
	std::vector<cv::Rect> faces;
	int i = 0;
	cv::CascadeClassifier c1;
	bool BMode;

	UPROPERTY(EditAnywhere, Category="数据模型")
	FString Mode="E:\\haarcascades\\haarcascade_frontalface_alt_tree.xml";

	
	FOnVideoTextureUpdate  OnVideoTextureUpdate;
	
	// UFUNCTION(BlueprintCallable, Category="测试")
	// void LoadImg();
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	cv::Mat frame;
	cv::VideoCapture stream;
	cv::Size size;

	//本地摄像头ID,从0开始,用于获取本地摄像头画面
	UPROPERTY(BlueprintReadWrite,EditAnywhere,Category=VideoCapturer)
	int32 CameraID;

	//网络视频流url,用于播放网络视频流
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = VideoCapturer)
	FString VideoURL;

	//用于控制打开本地摄像头还是网络视频流
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = VideoCapturer)
	bool bOpenWebVideo=true;

	//控制是否重置视频尺寸
	UPROPERTY(BlueprintReadWrite,EditAnywhere,Category=VideoCapturer)
	bool ShouldResize;

	//在ShouldResize为true时,用于更改视频尺寸
	UPROPERTY(BlueprintReadWrite,EditAnywhere,Category=VideoCapturer)
	FVector2D ResizeDeminsions;

	//视频刷新率,即帧数
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = VideoCapturer)
	float RefreshRate;

	//读取每帧视频画面的间隔,数值等于1/RefreshRate
	UPROPERTY(BlueprintReadWrite,EditAnywhere,Category=VideoCapturer)
	float RefreshTimer;

	//表示是否正在捕获画面
	UPROPERTY(BlueprintReadOnly,Category=VideoCapturer)
	bool isStreamOpen;

	//视频尺寸
	UPROPERTY(BlueprintReadWrite,Category=VideoCapturer)
	FVector2D VideoSize;

	/*视频纹理对象,每帧本地摄像头画面or视频流画面将会被更新到这个纹理对象
	 *定时获取该纹理对象,赋值给动态材质实例或者ui image控件,即可实现播放视频画面效果
	 */
	UPROPERTY(BlueprintReadOnly,Category=VideoCapturer)
	UTexture2D* VideoTexture;

	//视频颜色数据
	UPROPERTY(BlueprintReadOnly,Category=VideoCapturer)
	TArray<FColor>Data;

	//用于定时获取视频帧画面
	FTimerHandle TimerHandle;


	//1.初始化视频流
	void InitStream();

	//2.更新帧画面
	void UpdateFrame();

	//3.更新纹理对象
	void UpdateTexture();

	/**
	 *URL:视频流url,bOpenVideo为true时该参数被使用
	 *OnVideoTextureUpdate:视频画面更新时触发该回调
	 *bOpenVideo:true 播放URL的视频流;false 打开ID对应的摄像头
	 *ID:本地摄像头ID
	 */
	UFUNCTION(BlueprintCallable,Category=VideoCapturer)
	void StartCapture(const FString URL,FOnVideoTextureUpdate OnTextureUpdate,bool bOpenVideo=true,int32 ID=0);
};
