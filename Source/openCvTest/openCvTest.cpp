// Copyright Epic Games, Inc. All Rights Reserved.

#include "openCvTest.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, openCvTest, "openCvTest" );
