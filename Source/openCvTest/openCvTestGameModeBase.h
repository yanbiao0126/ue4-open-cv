// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "openCvTestGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class OPENCVTEST_API AopenCvTestGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
