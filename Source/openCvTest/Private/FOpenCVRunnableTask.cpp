// Fill out your copyright notice in the Description page of Project Settings.


#include "FOpenCVRunnableTask.h"

FOpenCVRunnableTask::~FOpenCVRunnableTask()
{
}

bool FOpenCVRunnableTask::Init()
{
	return IsValid(OpenCVActor);
}

uint32 FOpenCVRunnableTask::Run()
{
	GEngine->AddOnScreenDebugMessage(-1, 55.0f, FColor::Cyan, TEXT("执行多线程"));
	while (IsValid(OpenCVActor))
	{
// #if 1
// 		FScopeLock Lock(&CriticalSection);
// #endif

		if (OpenCVActor->isStreamOpen&&OpenCVActor->RefreshTimer>=1.0/OpenCVActor->RefreshRate)
		{
			OpenCVActor->RefreshTimer=0.f;
			OpenCVActor->UpdateFrame();
			OpenCVActor->UpdateFrame();
			OpenCVActor->UpdateTexture();
			OpenCVActor->OnVideoTextureUpdate.ExecuteIfBound(OpenCVActor->VideoTexture);
		}
	}
	return 0;
}

void FOpenCVRunnableTask::Exit()
{
	OpenCVActor = nullptr;
	FRunnable::Exit();
}
