// Fill out your copyright notice in the Description page of Project Settings.

#include "TestActor.h"


// Sets default values
ATestActor::ATestActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CameraID=0;
	RefreshRate=15;
	isStreamOpen=false;
	VideoSize=FVector2D::ZeroVector;
	ShouldResize=false;
	ResizeDeminsions=FVector2D(320,240);
	RefreshTimer=0.0f;
	stream=cv::VideoCapture();
	frame=cv::Mat();
}

ATestActor::~ATestActor()
{
	// if (RunnableTask && RunnableThread)
	// {
	// 	RunnableThread->Suspend(true); //暂停当前线程
	// 	RunnableTask->OpenCVActor = nullptr;        //标志位停止
	// 	RunnableThread->Suspend(false); //继续线程
	// 	RunnableThread->WaitForCompletion();  //等待线程运行结束
	// 	RunnableThread->Kill(true);     //将线程删除
	// 	delete RunnableTask;                // 删除 Runnable 实例
	// }
}

// Called when the game starts or when spawned
void ATestActor::BeginPlay()
{
	Super::BeginPlay();

	// GEngine->AddOnScreenDebugMessage(-1, 55.0f, FColor::Red, Mode);
	BMode = c1.load(TCHAR_TO_UTF8(*Mode));

	// FOpenCVRunnableTask* RunnableTask = new FOpenCVRunnableTask(this->GetName(), this);
	// FRunnableThread* RunnableThread = FRunnableThread::Create(RunnableTask, *RunnableTask->ThreadName);
}

void ATestActor::UpdateTextureRegions(UTexture2D* Texture, int32 MipIndex, uint32 NumRegions,
	FUpdateTextureRegion2D* Regions, uint32 SrcPitch, uint32 SrcBpp, uint8* SrcData, bool bFreeData)
{
	if (Texture->Resource)
	{
		struct FUpdateTextureRegionsData
		{
			FTexture2DResource* Texture2DResource;
			int32 MipIndex;
			uint32 NumRegions;
			FUpdateTextureRegion2D* Regions;
			uint32 SrcPitch;
			uint32 SrcBpp;
			uint8* SrcData;
		};
		FUpdateTextureRegionsData* RegionData=new FUpdateTextureRegionsData;

		RegionData->Texture2DResource=(FTexture2DResource*)Texture->Resource;
		RegionData->MipIndex=MipIndex;
		RegionData->NumRegions=NumRegions;
		RegionData->Regions=Regions;
		RegionData->SrcPitch=SrcPitch;
		RegionData->SrcBpp=SrcBpp;
		RegionData->SrcData=SrcData;
		//该宏在4.26之前和之后发生变更,老版本的引擎无法使用这个指令,以下为宏指令代码
		ENQUEUE_RENDER_COMMAND(UpdateTextureRegionsData)([RegionData, bFreeData](FRHICommandListImmediate& RHICmdList) {

			for (uint32 RegionIndex = 0; RegionIndex < RegionData->NumRegions; ++RegionIndex)
			{
				int32 CurrentFirstMip = RegionData->Texture2DResource->GetCurrentFirstMip();
				if (RegionData->MipIndex >= CurrentFirstMip)
				{
					RHIUpdateTexture2D(
						RegionData->Texture2DResource->GetTexture2DRHI(),
						RegionData->MipIndex - CurrentFirstMip,
						RegionData->Regions[RegionIndex],
						RegionData->SrcPitch,
						RegionData->SrcData
						+ RegionData->Regions[RegionIndex].SrcY * RegionData->SrcPitch
						+ RegionData->Regions[RegionIndex].SrcX * RegionData->SrcBpp
					);
				}
			}
			if (bFreeData)
			{
				FMemory::Free(RegionData->Regions);
				FMemory::Free(RegionData->SrcData);
			}
			delete RegionData;
			});
		//以上为宏指令代码
	}
}

// void ATestActor::LoadImg()
// {
// 	/*cv::imread("1./jpg");*/
// 	cv::Mat image = cv::imread("C:\\Users\\yanbi\\Pictures\\Feedback\\{D2E9B91F-B345-4319-931F-72AF9E2CCF8A}\\Capture001.png");
// 	cv::imshow("Test", image);
// }

// Called every frame
void ATestActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//每隔一定时间更新视频纹理对象
	RefreshTimer+=DeltaTime;
	if (isStreamOpen&&RefreshTimer>=1.0/RefreshRate)
	{
		RefreshTimer=0.f;
		UpdateFrame();
		UpdateTexture();
		OnVideoTextureUpdate.ExecuteIfBound(VideoTexture);
	}

}

void ATestActor::InitStream()
{
	//1.判断开启网络视频流还是本地摄像头
	if (bOpenWebVideo)
	{
		stream.open(TCHAR_TO_UTF8(*VideoURL));
	}
	else
	{
		stream.open(CameraID);
	}

	//2.判断是否开启成功
	if (stream.isOpened())
	{
		isStreamOpen=true;
		UpdateFrame();
		//3.初始化视频纹理对象
		VideoSize=FVector2D(frame.cols,frame.rows);
		size=cv::Size(ResizeDeminsions.X,ResizeDeminsions.Y);
		VideoTexture=UTexture2D::CreateTransient(VideoSize.X,VideoSize.Y);
		VideoTexture->UpdateResource();
		VideoUpdateTextureRegion=new FUpdateTextureRegion2D(0,0,0,0,VideoSize.X,VideoSize.Y);
		Data.Init(FColor(0,0,0,255),VideoSize.X*VideoSize.Y);

		UpdateTexture();
		//开启成功之后清除定时器
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle);
	}
}

void ATestActor::UpdateFrame()
{
	//1.每次更新视频帧画面,都会清空就得frame数据
	if (stream.isOpened())
	{
		frame.release();
		//2.从视频流中将视频帧画面读取到frame中
		stream.read(frame);
		//3.更新视频尺寸,根据需求决定是否重新设置尺寸
		if (ShouldResize)
		{
			cv::resize(frame,frame,size);
		}
		
		if (BMode)
		{
			// GEngine->AddOnScreenDebugMessage(-1, 666.0,FColor::Red, TEXT("人脸检测"));
			dst = cv::Mat::zeros(frame.size(),frame.type());
			cvtColor(frame,dst,cv::COLOR_BGR2GRAY);
			dst1 = cv::Mat::zeros(dst.size(),dst.type());
			equalizeHist(dst,dst1);
			c1.detectMultiScale(dst1,faces,1.1,3,0,cv::Size(50,50));
			for(i = 0;i < faces.size();i++)
			{
				FAIUsers aiUser;
				aiUser.ID = i;
				aiUser.XX = faces[i].x;
				aiUser.YY = faces[i].y;
				if (AiUsers.Num() < i+1)
				{
					AiUsers.Add(aiUser);
				} else
				{
					AiUsers[i] = aiUser;
				}
				
				rectangle(frame,faces[i],cv::Scalar(0,0,255),2,cv::LINE_8,0);
				image = frame(cv::Rect(faces[i].x,faces[i].y,faces[i].width, faces[i].height));
				resize(image,imagel,cv::Size(100,100));
				cvtColor(imagel,imagel,cv::COLOR_BGR2GRAY);
			}

			

			
			// int i = 0;
			// cv::Mat dst;
			// cv::Mat dst1;
			// std::vector<cv::Rect> faces;
			// cv::Mat image;
			// cv::Mat imagel;
			//
			// dst = cv::Mat::zeros(frame.size(),frame.type());
			// cv::cvtColor(frame, dst, 1);
			// dst1 = cv::Mat::zeros(dst.size(),dst.type());
			// cv::equalizeHist(dst,dst1);
			// c1.detectMultiScale(dst1, faces);
			// for (i=0;i<faces.size();i++)
			// {
			// 	cv::rectangle(frame, faces[i],cv::Scalar(0,0,255),2,cv::LINE_8,0);
			// 	image = frame(cv::Rect(faces[i].x,faces[i].y,faces[i].width, faces[i].height));
			// 	cv::resize(image, imagel, cv::Size(100,100));
			// 	
			// }
		} else
		{
			
		}
	}
	else
	{
		isStreamOpen=false;
	}
}

void ATestActor::UpdateTexture()
{
	if (isStreamOpen && frame.data)
	{
		for (int y = 0; y < VideoSize.Y; y++)
		{
			for (int x = 0; x < VideoSize.X; x++)
			{
				int l = x + (y * VideoSize.X);
				Data[l].B = frame.data[l * 3 + 0];
				Data[l].G = frame.data[l * 3 + 1];
				Data[l].R = frame.data[l * 3 + 2];
			}
		}
		UpdateTextureRegions(VideoTexture, (int32)0, (uint32)1, VideoUpdateTextureRegion, (uint32)(4 * VideoSize.X), (uint32)4, (uint8*)Data.GetData(), false);
	}
}

void ATestActor::StartCapture(const FString URL, FOnVideoTextureUpdate OnTextureUpdate, bool bOpenVideo, int32 ID)
{
	VideoURL=URL;
	OnVideoTextureUpdate=OnTextureUpdate;
	bOpenWebVideo=bOpenVideo;
	CameraID=ID;
	InitStream();
	//如果视频流开启失败,使用定时器尝试重新初始化
	if (!stream.isOpened())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle,this,&ATestActor::InitStream,5.f,true);
	}
}